msgid ""
msgstr ""
"Project-Id-Version: gpxmotion\n"
"Report-Msgid-Bugs-To: translations\\@example.com\n"
"POT-Creation-Date: 2020-05-04 19:56+0200\n"
"PO-Revision-Date: 2020-09-28 18:04\n"
"Last-Translator: \n"
"Language-Team: Korean\n"
"Language: ko_KR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: gpxmotion\n"
"X-Crowdin-Project-ID: 293767\n"
"X-Crowdin-Language: ko\n"
"X-Crowdin-File: /master/translationfiles/templates/gpxmotion.pot\n"
"X-Crowdin-File-ID: 125\n"

#: /var/www/html/n17/apps/gpxmotion/appinfo/app.php:43
#: /var/www/html/n17/apps/gpxmotion/specialAppInfoFakeDummyForL10nScript.php:2
msgid "GpxMotion"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/filetypes.js:22
msgid "View in GpxMotion"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/filetypes.js:37
msgid "Edit with GpxMotion"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:8
msgid "no vehicle"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:9
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:33
msgid "plane"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:10
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:36
msgid "car"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:11
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:35
msgid "foot"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:12
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:34
msgid "bike"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:13
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:38
msgid "train"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:14
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:37
msgid "bus"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:15
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:39
msgid "kayak"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:16
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:40
msgid "boat"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:295
msgid "Tile server \"{ts}\" has been deleted"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:298
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:302
msgid "Failed to delete tile server \"{ts}\""
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:317
msgid "Server name or server url should not be empty"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:318
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:323
msgid "Impossible to add tile server"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:322
msgid "A server with this name already exists"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:355
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:136
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:179
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:224
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:273
msgid "Delete"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:388
msgid "Tile server \"{ts}\" has been added"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:391
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:395
msgid "Failed to add tile server \"{ts}\""
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:419
msgid "File successfully saved as"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:448
msgid "Impossible to load this file. "
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:449
msgid "Supported format : gpx"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:450
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1097
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1099
msgid "Load error"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:676
msgid "Number of tracks/routes"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:678
msgid "Vehicle"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:690
msgid "Duration (sec)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:692
msgid "Section title"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:693
msgid "Guess title from track/route name"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:698
msgid "Color"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:700
msgid "Description"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:702
msgid "Picture URL"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:704
msgid "Detail URL"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:706
msgid "Starting point title"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:708
msgid "Starting point description"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:710
msgid "Starting point picture URL"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:712
msgid "Starting point detail URL"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:717
msgid "Remove section"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:719
msgid "Zoom on section"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:721
msgid "Insert section before"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:723
msgid "Insert section after"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:776
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:783
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:790
msgid "Impossible to write file"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:777
msgid "Write access denied"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:784
msgid "Folder does not exist"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:791
msgid "Folder write access denied"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:797
msgid "Bad file name, must end with \".gpx\""
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:1044
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:960
msgid "Load file (gpx)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:1072
msgid "There is nothing to save"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:1079
msgid "Where to save"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionEdit.js:1107
msgid "Load and view animation file (gpx)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:160
msgid "All sections synchronized"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:164
msgid "All sections"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:292
msgid "no title"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:294
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1474
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1487
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1491
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1495
msgid "Section"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:626
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:5
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:99
msgid "Options"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:628
msgid "loop"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:630
msgid "autozoom"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:633
msgid "Legend"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:638
msgid "Pins"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:640
msgid "start"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:641
msgid "step"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:642
msgid "end"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:858
msgid "Edit current file"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:872
msgid "Share current file"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:874
msgid "Public link to"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:922
msgid "This public link will work only if \"{title}\" or one of its parent folder is shared in \"files\" app by public link without password"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:954
msgid "Load file"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:979
msgid "Restrict animation to current view"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:992
msgid "Next section (n)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1005
msgid "Previous section (p)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1018
msgid "Draw complete trip (g)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1034
msgid "Reset (i)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1051
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1064
msgid "Play/Pause animation (spacebar)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1096
msgid "No animation data found in the GPX file"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1274
msgid "Nothing to display"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1479
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1520
msgid "Click on the picture to see it fullsize"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1479
msgid "Picture from the track"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1483
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1525
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1579
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1588
msgid "More about"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1488
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1492
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1496
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1530
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1558
msgid "Click for more details"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1515
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1528
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1554
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1557
#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1570
msgid "Step"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1520
msgid "Picture from the starting point"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1571
msgid "final"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1616
msgid "Ready to play"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/gpxMotionView.js:1772
msgid "Load a file to display an animation"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/leaflet.js:5
msgid "left"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/js/leaflet.js:5
msgid "right"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/specialAppInfoFakeDummyForL10nScript.php:3
msgid " "
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:4
msgid "Load and save files"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:6
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:284
msgid "About GpxMotion"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:23
msgid "Load and view file"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:24
msgid "Load and edit file"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:28
msgid "Save"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:30
msgid "File name"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:34
msgid "Choose directory and save"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:38
msgid "Preview current animation"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:44
msgid "Use real time proportions"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:47
msgid "Play all sections simultaneously"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:50
msgid "Play all sections synchronized in real time"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:52
msgid "Total duration (sec)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:55
msgid "Clear animation sections"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:56
msgid "Add animation section"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:67
msgid "loading file"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:71
msgid "exporting file to gpx"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:75
msgid "saving file"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:102
msgid "Clear sections before loading"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:106
msgid "Custom tile servers"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:109
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:148
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:191
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:236
msgid "Server name"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:110
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:149
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:192
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:237
msgid "For example : my custom server"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:111
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:150
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:193
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:238
msgid "Server url"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:112
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:194
msgid "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:113
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:152
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:195
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:240
msgid "Min zoom (1-20)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:115
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:154
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:197
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:242
msgid "Max zoom (1-20)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:117
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:160
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:205
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:254
msgid "Add"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:120
msgid "Your tile servers"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:145
msgid "Custom overlay tile servers"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:151
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:239
msgid "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:156
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:244
msgid "Transparent"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:158
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:246
msgid "Opacity (0.0-1.0)"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:163
msgid "Your overlay tile servers"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:188
msgid "Custom WMS tile servers"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:199
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:248
msgid "Format"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:201
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:250
msgid "WMS version"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:203
#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:252
msgid "Layers to display"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:208
msgid "Your WMS tile servers"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:233
msgid "Custom WMS overlay servers"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:257
msgid "Your WMS overlay tile servers"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:286
msgid "Features overview"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:288
msgid "Shortcuts"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:290
msgid "toggle sidebar"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:293
msgid "Documentation"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:301
msgid "Source management"
msgstr ""

#: /var/www/html/n17/apps/gpxmotion/templates/editcontent.php:312
msgid "Authors"
msgstr ""

