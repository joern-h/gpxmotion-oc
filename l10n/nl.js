OC.L10N.register(
    "gpxmotion",
    {
    "View in GpxMotion" : "Weergeven in GpxMotion",
    "Edit with GpxMotion" : "Bewerken met GpxMotion",
    "no vehicle" : "geen voertuig",
    "plane" : "vliegtuig",
    "car" : "auto",
    "foot" : "voet",
    "bike" : "fiets",
    "train" : "trein",
    "Tile server \"{ts}\" has been deleted" : "Tegel server \"{ts}\" is verwijderd",
    "Failed to delete tile server \"{ts}\"" : "Fout bij het verwijderen van de tegel server \"{ts}\"",
    "Server name or server url should not be empty" : "De servernaam of server-url mag niet leeg zijn",
    "Impossible to add tile server" : "Onmogelijk om de tegel server toe te voegen",
    "A server with this name already exists" : "Een server met deze naam bestaat al",
    "Delete" : "Verwijderen",
    "Tile server \"{ts}\" has been added" : "Tegel server \"{ts}\" is toegevoegd",
    "Failed to add tile server \"{ts}\"" : "Tegel server \"{ts}\" kon niet worden toegevoegd",
    "File successfully saved as" : "Bestand is succesvol opgeslagen als",
    "Impossible to load this file. " : "Onmogelijk om het bestand te laden. ",
    "Supported format : gpx" : "Ondersteunde formaat: gpx",
    "Load error" : "Fout tijdens het laden",
    "Number of tracks/routes" : "Aantal tracks/routes",
    "Vehicle" : "Voertuig",
    "Duration (sec)" : "Duur (sec)",
    "Section title" : "Sectietitel",
    "Guess title from track/route name" : "Gok titel van track/route naam",
    "Color" : "Kleur",
    "Description" : "Omschrijving",
    "Picture URL" : "Afbeeldingslink",
    "Detail URL" : "Detaillink",
    "Starting point title" : "Startpunt",
    "Starting point description" : "Begin punt omschrijving",
    "Starting point picture URL" : "Begin punt afbeeldinglink",
    "Starting point detail URL" : "Begin punt detaillink",
    "Remove section" : "Sectie verwijderen",
    "Zoom on section" : "Zoom op Sectie",
    "Insert section before" : "Sectie invoegen vóór",
    "Insert section after" : "Sectie invoegen na",
    "Impossible to write file" : "Onmogelijk om bestand te schrijven",
    "Write access denied" : "Schrijftoegang geweigerd",
    "Folder does not exist" : "Map bestaat niet",
    "Folder write access denied" : "Schrijftoegang op map geweigerd",
    "Bad file name, must end with \".gpx\"" : "Onjuiste bestandsnaam, moet eindigen op \".gpx\"",
    "Load file (gpx)" : "Laad (gpx) bestand",
    "There is nothing to save" : "Er is niets om op te slaan",
    "Where to save" : "Waar opslaan",
    "Load and view animation file (gpx)" : "Laat en bekijk animatiebestand (gpx)",
    "All sections synchronized" : "Alle secties gesynchroniseerd",
    "All sections" : "Alle secties",
    "no title" : "Geen titel",
    "Section" : "Sectie",
    "Options" : "Optie",
    "loop" : "Herhalen",
    "autozoom" : "Autozoom",
    "Legend" : "Legenda",
    "Pins" : "Punaise",
    "step" : "stap",
    "end" : "einde",
    "Edit current file" : "Huidig bestand bewerken",
    "Share current file" : "Huidig bestand delen",
    "Public link to" : "Openbare link naar",
    "This public link will work only if \"{title}\" or one of its parent folder is shared in \"files\" app by public link without password" : "Deze openbare verbinding zal alleen werken als \"{title}\" of een van de bovenliggende map in 'bestanden' app wordt gedeeld door openbare verbinding zonder wachtwoord",
    "Load file" : "Bestand laden",
    "Restrict animation to current view" : "Animatie naar huidige weergave beperken",
    "Next section (n)" : "Volgende hoofdstuk",
    "Previous section (p)" : "Vorige sectie (p)",
    "Draw complete trip (g)" : "Teken complete reis (g)",
    "Reset (i)" : "Resetten (i)",
    "Play/Pause animation (spacebar)" : "Animatie afspelen/pauzeren (spatiebalk)",
    "No animation data found in the GPX file" : "Geen animatie gegevens gevonden in het GPX-bestand",
    "Nothing to display" : "Niets om weer te geven",
    "More about" : "Meer over",
    "Click for more details" : "Klik voor meer informatie",
    "Step" : "Stap",
    "final" : "Laatste",
    "Ready to play" : "Klaar om te spelen",
    "Load a file to display an animation" : "Laad een bestand om een animatie weer te geven",
    "left" : "links",
    "right" : "rechts",
    "Load and save files" : "Laden en opslaan van bestanden",
    "About GpxMotion" : "Over GpxMotion",
    "Load and view file" : "Bestand laden en weergeven",
    "Load and edit file" : "Bestand laden en bewerken",
    "Save" : "Oplsaan",
    "File name" : "Bestandsnaam",
    "Choose directory and save" : "Kies map en sla op",
    "Preview current animation" : "Voorbeeld huidige animatie",
    "Use real time proportions" : "Gebruik van realistiche verhoudingen",
    "Play all sections simultaneously" : "Alle secties tegelijk spelen",
    "Play all sections synchronized in real time" : "Spelen van alle secties gesynchroniseerd in real-time",
    "Total duration (sec)" : "Totale duur (sec)",
    "Clear animation sections" : "Animatie secties wissen",
    "Add animation section" : "Animatie sectie toevoegen",
    "loading file" : "bestand laden",
    "exporting file to gpx" : "bestand exporteren naar gpx",
    "saving file" : "bestand opslaan",
    "Clear sections before loading" : "Secties wissen voor het laden",
    "Custom tile servers" : "Aangepaste tegel servers",
    "Server name" : "Servernaam",
    "For example : my custom server" : "Bijvoorbeeld : mijn gepersonaliseerde server",
    "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png" : "Bijvoorbeeld : http://tile.server.org/cycle/{z}/{x}/{y}.png",
    "Add" : "Toevoegen",
    "Your tile servers" : "Uw tegel-servers",
    "Custom overlay tile servers" : "Aangepaste overlay tegel servers",
    "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png" : "Bijvoorbeeld : http://overlay.server.org/cycle/{z}/{x}/{y}.png",
    "Transparent" : "Doorzichtig",
    "Opacity (0.0-1.0)" : "Doorzichtigheid (0.0-1.0)",
    "Your overlay tile servers" : "Jouw overlay tile servers",
    "Custom WMS tile servers" : "Aangepaste WMS tegel servers",
    "Format" : "Indeling",
    "WMS version" : "WMS versie",
    "Layers to display" : "Lagen om weer te geven",
    "Your WMS tile servers" : "Uw WMS tegel servers",
    "Custom WMS overlay servers" : "Aangepaste WMS overlay servers",
    "Your WMS overlay tile servers" : "Uw WMS overlay tegel servers",
    "Features overview" : "Overzicht van functies",
    "Shortcuts" : "Snelkoppelingen",
    "toggle sidebar" : "zijbalk tonen/verbergen",
    "Documentation" : "Documentatie",
    "Source management" : "Bron beheer",
    "Authors" : "Auteurs"
},
"nplurals=2; plural=(n != 1);");
