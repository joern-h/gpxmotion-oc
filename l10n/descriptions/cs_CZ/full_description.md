# Nextcloud aplikace GpxMotion

GpxMotion je Nextcloud aplikace pro vytváření a zobrazení animací cesty na interaktivní mapě.

Chcete-li sledovat animaci, klepněte na tlačítko „Načíst a zobrazit soubor“ na hlavní stránce GpxMotion. Pokud nebyly nastaveny žádné informace o animaci, zobrazí se výchozí (jedna část 10 sekund na každou stopu/trasy). Pokud v sekci animace není dostatek času a je povoleno „použít poměrný počet hodin v reálném čase“ (výchozí hodnota, je), trvání animace se nezmění, ale rychlost animace bude úměrná reálné rychlosti.

Chcete-li definovat animaci, přejděte na hlavní stránku GpxMotion a nahrajte GPX soubor, který už obsahuje objednané stopy/trasy. Pak definujte kroky animace. Potom zkontrolujte, zda jste spokojeni s náhledem animace. Potom uložte výsledek do GPX souboru (data animace jsou uložena jako JSON v poli popisu GPX).

Pokud je soubor veřejně sdílen bez hesla v aplikaci „Soubory“, můžete vytvořit veřejný odkaz GpxMotion na jeho animaci pomocí tlačítka „Sdílet“ na stránce „view“.

Tato aplikace je testována na Nextcloudu 16 s Firefoxem a Chromiem.

Jakákoliv zpětná vazba bude vítána.

Pokud chcete pomoci přeložit tuto aplikaci do svého jazyka, přejděte na [GpxEdit Crowdin projekt](https://crowdin.com/project/gpxmotion).

## Instalace

Podrobnosti ohledně instalace a napojení na aplikaci „Soubory“ naleznete v [AdminDoc](https://gitlab.com/eneiluj/gpxmotion-oc/wikis/admindoc).

## Alternativy

Pokud hledáte alternativy, podívejte se na:
- [gpxanim](https://github.com/rvl/gpxanim) vytvoří video soubor
- [Gpxanim](http://zdila.github.io/gpx-animator/) vytvoří video soubor


